$(document).on('click', '.ems-add', function() {
	showForm();
});

$(document).on('click', '.ems-show', function() {
	showDataTable();
});

function showForm() {
	$('.ems-add-div').fadeOut("fast");
	$('.ems-list-div').delay(100).fadeIn();
}

function showDataTable() {
	$('.ems-list-div').fadeOut("fast");
	$('.ems-add-div').delay(100).fadeIn();
}