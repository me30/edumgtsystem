<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%-- <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%> --%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Login page</title>
<link href="resources/css/bootstrap.min.css" rel="stylesheet"></link>
<link rel="stylesheet" type="text/css" href="resources/font-awesome-4.3.0/css/font-awesome.min.css" />

<script type="text/javascript"  src="resources/js/bootstrap.min.js"></script>
<script type="text/javascript"  src="resources/js/jquery-1.7.min.js"></script>
<script type="text/javascript"  src="resources/devJs/login.js"></script>


<style type="text/css">
.vertical-center {
  min-height: 100%;  /* Fallback for browsers do NOT support vh unit */
  min-height: 100vh; /* These two lines are counted as one :-)       */

  display: flex;
  align-items: center;
}
</style>
</head>
<body>
	<div id="mainWrapper vertical-center">
		<div class="container "  style="margin-top: 30px; margin-left: 35%; position: relative; top: 150px;">
			<div class="col-md-4">
				<div class="login-panel panel panel-default">
					<div class="panel-heading">
						<h3 class="panel-title">Sign In</h3>
					</div>
					<div class="panel-body">
						<form role="form">
							<fieldset>
								<div class="form-group">
									<input class="form-control" id="loginEmail" placeholder="E-mail" name="email" type="email" autofocus="">
								</div>
								<div class="form-group">
									<input class="form-control" id="loginPassword" placeholder="Password" name="password" type="password" value="">
								</div>
								<div class="checkbox">
									<label> <input name="remember" type="checkbox" value="Remember Me">Remember Me
									</label>
								</div>
								<!-- Change this to a button or input when using this as a form -->
								<a href="javascript:formReset();;" class="btn btn-sm btn-danger">Reset</a>
								<a href="javascript:makeLogin();" class="btn btn-sm btn-success">Login</a>
							</fieldset>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</body>
</html>