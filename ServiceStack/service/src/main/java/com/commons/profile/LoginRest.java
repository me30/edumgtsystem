package com.commons.profile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;

import com.commons.security.AppUserDetails;
import com.commons.security.TokenUtils;
import com.commons.security.UserAuthenticationProvider;
import com.commons.app.web.models.UserLogin;
import com.commons.bal.profile.AuthorizationsBal;

@Component
@Path("/login")
public class LoginRest {

	@Context
	HttpHeaders headers;

	@Context
	HttpServletResponse response;

	@Context
	HttpServletRequest request;

	@Autowired
	AuthorizationsBal authorizationsBal;
	
	@Autowired
	UserAuthenticationProvider userAuthenticationProvider;
	
	@POST
    @Path("/authentication")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response authentication(UserLogin userLogin) {
        try {
        	
        	AppUserDetails userDetails = authorizationsBal.getAuthoriztionDetails(userLogin.getEmail(),userLogin.getPwd());
        	
        	if(null == userDetails){
        		throw new UsernameNotFoundException("User not exist");
        	}
        	UsernamePasswordAuthenticationToken authenticationToken = new UsernamePasswordAuthenticationToken(userDetails.getEmailAddr(), userDetails.getPassword());
    		Authentication authentication = userAuthenticationProvider.authenticate(authenticationToken);
    		SecurityContextHolder.getContext().setAuthentication(authentication);

    		/*
    		 * Reload user as password of authentication principal will be null after authorization and
    		 * password is needed for token generation
    		 */
    		
    		userDetails.setToken(TokenUtils.createToken(userDetails));
    		userDetails.setPassword(";)");
            return Response.status(200).entity(userDetails).build();
        } catch (UsernameNotFoundException e) {
        	e.printStackTrace();
            throw new WebApplicationException(e.getMessage());
        } catch (Exception e) {
        	e.printStackTrace();
            throw new WebApplicationException(e.getMessage());
        }

    }
	
	@POST
    @Path("/logout")
    @Produces(MediaType.APPLICATION_JSON)
    public Response lotout() {
        try {
        	HttpSession session = request.getSession();
        	session.invalidate();
    	    Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
    	    if (authentication.isAuthenticated()){
    	        authentication.setAuthenticated(false);
    	    }
    	        
            return Response.status(200).build();
        } catch (UsernameNotFoundException e) {
        	e.printStackTrace();
            throw new WebApplicationException(e.getMessage());
        } catch (Exception e) {
        	e.printStackTrace();
            throw new WebApplicationException(e.getMessage());
        }

    }
}
