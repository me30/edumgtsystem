package com.commons.configapp;

import javax.ws.rs.ext.ContextResolver;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;


public class JacksonJsonProvider implements ContextResolver<ObjectMapper> {

	final ObjectMapper mapper;

	

	public JacksonJsonProvider() {
		 this.mapper = new ObjectMapper();
	        this.mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
	        this.mapper.configure(DeserializationFeature.ACCEPT_EMPTY_STRING_AS_NULL_OBJECT, true);
	    }

	@Override
	public ObjectMapper getContext(Class<?> type) {
		return this.mapper;
	}
}
