package com.commons.configapp;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.commons.appmodels.AppException;

public class CustomExceptionMapper implements
		ExceptionMapper<WebApplicationException> {

	@Context
	private HttpHeaders headers;

	public Response toResponse(WebApplicationException exception) {
		Logger log = LogManager.getLogger(exception.getClass());
		int status;
		AppException payload;

		if (null != exception.getMessage()
				&& exception.getMessage().equalsIgnoreCase("Timeout Expired")) {
			status = 401;
			payload = new AppException(401, exception.getMessage());
			payload.setServerMessage("Oops!!Seems like your session has expired.");

		} else {
			status = exception.getResponse().getStatus();
			payload = new AppException(exception.getResponse().getStatus(),
					exception.getMessage());
			switch (exception.getResponse().getStatus()) {
			case 400: {
				payload.setServerMessage("Oh Snap!!" + exception.getMessage());
				break;
			}
			case 403: {
				payload.setServerMessage("Oops!!Resource not found");
				break;
			}
			case 404: {
				payload.setServerMessage("Oops!!This is not the resource you are looking for");
				break;
			}
			case 500: {
				payload.setServerMessage("Oops!!Looks like something went wrong");
				break;
			}
			case 409: {
				payload.setServerMessage("Oh Snap!!" + exception.getMessage());
				break;
			}
			default: {
				payload.setServerMessage("Oops!!Seems like we ran into some trouble");
				break;
			}
			}
		}
		log.log(Level.ERROR, payload.getServerMessage(), exception);
		return Response.status(status).entity(payload)
				.type(MediaType.APPLICATION_JSON_TYPE).build();
	}

}
