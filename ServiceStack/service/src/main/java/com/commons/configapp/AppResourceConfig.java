package com.commons.configapp;

import org.glassfish.jersey.jackson.JacksonFeature;
import org.glassfish.jersey.media.multipart.MultiPartFeature;
import org.glassfish.jersey.server.ResourceConfig;

import com.commons.filter.CorsResponseFilter;

public class AppResourceConfig extends ResourceConfig {
	public AppResourceConfig() {
		this.packages("com.commons.profile;com.commons.filter;org.codehaus.jackson.jaxrs");
		this.register(JacksonJsonProvider.class);
		this.register(JacksonFeature.class);
		this.register(MultiPartFeature.class);
		this.register(CustomExceptionMapper.class);
		this.register(CorsResponseFilter.class);

	}
}
