package com.commons.dal.db;

import org.hibernate.Session;
import org.hibernate.Transaction;

public class Operations {
    private Session session;
    private Transaction transaction;

    public Operations() {

    }

    public Operations(Session session, Transaction transaction) {
        this.session = session;
        this.transaction = transaction;
    }

    public Session getSession() {
        return session;
    }

    public void setSession(Session session) {
        this.session = session;
    }

    public Transaction getTransaction() {
        return transaction;
    }

    public void setTransaction(Transaction transaction) {
        this.transaction = transaction;
    }
}
