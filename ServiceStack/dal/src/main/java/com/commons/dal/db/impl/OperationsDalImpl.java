package com.commons.dal.db.impl;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import com.commons.dal.db.Operations;
import com.commons.dal.db.OperationsDal;
import com.commons.db.models.CoreQuery;
import com.commons.db.utils.session.SessionUtil;
import com.commons.models.enums.Databases;

import java.util.List;

public class OperationsDalImpl implements OperationsDal {
    private static OperationsDal appDal;

    private SessionFactory sessionFactory;

    private OperationsDalImpl(Databases database) {
        switch (database) {
            case APP_DB:
                sessionFactory = SessionUtil.getSessionFactory();
                break;
            default:
                break;
        }
    }

    public static OperationsDal getInstance(Databases database) {
        switch (database) {
            case APP_DB:
                if (appDal == null) {
                    appDal = new OperationsDalImpl(database);
                }
                return appDal;
            default:
                return null;
        }
    }

    @Override
    public boolean saveOrUpdate(Object object, Operations operations) throws Exception {
        operations.getSession().saveOrUpdate(object);
        return true;
    }

    @Override
    public boolean evict(Object object, Operations operations) throws Exception {
        operations.getSession().evict(object);
        return true;
    }

    @Override
    public Object save(Object object, Operations operations) throws Exception {
        return operations.getSession().save(object);
    }

    @Override
    public boolean delete(Object object, Operations operations) throws Exception {
        operations.getSession().delete(object);
        return true;
    }

    @Override
    public void executeQuery(CoreQuery query, Operations operations) throws Exception {
        OperationCommons.getSessionQuery(query, operations.getSession()).executeUpdate();
    }

    @Override
    public List fetch(CoreQuery query, Operations operations) throws Exception {
        return OperationCommons.getSessionQuery(query, operations.getSession()).list();
    }

    public int getRecordCount(CoreQuery query, Operations operations) throws Exception {
        Object value = OperationCommons.getSessionQuery(query, operations.getSession()).uniqueResult();
        if (value != null) {
            return ((Long) value).intValue();
        } else {
            return 0;
        }
    }

    public Operations beginTransaction() throws Exception {
        Session session = sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();
        return new Operations(session, transaction);
    }

    public void rollBackTransaction(Operations operations) throws Exception {
        if (operations.getTransaction() != null) {
            operations.getTransaction().rollback();
        }
        if (operations.getSession() != null && operations.getSession().isOpen()) {
            operations.getSession().close();
        }
    }

    public void commitTransaction(Operations operations) throws Exception {
        if (operations.getTransaction() != null) {
            operations.getTransaction().commit();
        }
        if (operations.getSession() != null && operations.getSession().isOpen()) {
            operations.getSession().close();
        }
    }
}
