package com.commons.dal.db;

import java.util.List;

import com.commons.db.models.CoreQuery;

public interface OperationsDal {
    boolean saveOrUpdate(Object object, Operations operations) throws Exception;

    boolean evict(Object object, Operations operations) throws Exception;

    Object save(Object object, Operations operations) throws Exception;

    boolean delete(Object object, Operations operations) throws Exception;

    void executeQuery(CoreQuery query, Operations operations) throws Exception;

    List fetch(CoreQuery query, Operations operations) throws Exception;

    int getRecordCount(CoreQuery query, Operations operations) throws Exception;

    void rollBackTransaction(Operations operations) throws Exception;

    void commitTransaction(Operations operations) throws Exception;

    Operations beginTransaction() throws Exception;
}
