package com.commons.dal.profile;

import com.commons.entity.MmUser;

public interface UserMasterDal {
	MmUser userRegistration(MmUser appUserDetails) throws Exception;
	
	boolean isUserExist(String email) throws Exception;
}
