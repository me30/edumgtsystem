package com.commons.dal.profile.impl;

import org.springframework.stereotype.Repository;

import com.commons.dal.db.Operations;
import com.commons.dal.db.OperationsDal;
import com.commons.dal.db.impl.OperationsDalImpl;
import com.commons.dal.profile.UserMasterDal;
import com.commons.db.models.CoreQuery;
import com.commons.entity.MmUser;
import com.commons.exceptions.DuplicateEntryException;
import com.commons.models.enums.Databases;


@Repository
public class UserMasterDalImpl implements UserMasterDal {

	OperationsDal ops;

	public UserMasterDalImpl() {
		ops = OperationsDalImpl.getInstance(Databases.APP_DB);
	}

	@Override
	public MmUser userRegistration(MmUser appUserDetails) throws Exception {
		Operations op = ops.beginTransaction();
		MmUser mmUser = null;
		try {
			if(null != appUserDetails.getId() && appUserDetails.getId()>0){
				ops.saveOrUpdate(appUserDetails, op);
			}else{
				mmUser =  (MmUser) ops.save(appUserDetails, op);
			}
			ops.commitTransaction(op);
			return mmUser;
		} catch (Exception e) {
			e.printStackTrace();
			ops.rollBackTransaction(op);
			throw e;
		}
	}

	@Override
	public boolean isUserExist(String email) throws Exception {
		Operations op = ops.beginTransaction();
		try {
			CoreQuery coreQuery = new CoreQuery("Select count(1) from MmUser where emailAddr = :email ",true);
			coreQuery.addParam("email", email);
			
			int i = ops.getRecordCount(coreQuery, op);
			ops.commitTransaction(op);
			if(i == 0)
				return false;
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			ops.rollBackTransaction(op);
			throw new DuplicateEntryException("already exist");
		}
	}

}
