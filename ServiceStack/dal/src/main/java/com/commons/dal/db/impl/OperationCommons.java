package com.commons.dal.db.impl;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;

import com.commons.db.models.CoreQuery;
import com.commons.db.models.CoreQueryParam;
import com.commons.utility.Validator;

public class OperationCommons {
    public static Query getSessionQuery(CoreQuery query, Session session) {

        Query sessionQuery = session.createQuery(query.getQueryString());


        if (query.getStartFrom() >= 0 && query.getMaxCount() >= 0) {
            sessionQuery.setFirstResult(query.getStartFrom()).setMaxResults(query.getMaxCount());
        } else if (query.getStartFrom() < 0 && query.getMaxCount() >= 0) {
            sessionQuery.setMaxResults(query.getMaxCount());
        } else if (query.getStartFrom() >= 0 && query.getMaxCount() < 0) {
            sessionQuery.setFirstResult(query.getStartFrom());
        }
        if (Validator.validateList(query.getQueryParams())) {
            for (CoreQueryParam param : query.getQueryParams()) {
                if (param.getParamValue() instanceof List) {
                    sessionQuery.setParameterList(param.getParamName(), (List) param.getParamValue());
                } else {
                    sessionQuery.setParameter(param.getParamName(), param.getParamValue());
                }
            }
        }
        return sessionQuery;
    }
}
