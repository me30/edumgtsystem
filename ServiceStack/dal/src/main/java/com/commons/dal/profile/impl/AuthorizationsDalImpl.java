package com.commons.dal.profile.impl;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.commons.dal.db.Operations;
import com.commons.dal.db.OperationsDal;
import com.commons.dal.db.impl.OperationsDalImpl;
import com.commons.dal.profile.AuthorizationsDal;
import com.commons.db.models.CoreQuery;
import com.commons.entity.MmUser;
import com.commons.models.enums.Databases;
import com.commons.utility.Validator;

@Repository
public class AuthorizationsDalImpl implements AuthorizationsDal {

	OperationsDal ops;

	public AuthorizationsDalImpl() {
		ops = OperationsDalImpl.getInstance(Databases.APP_DB);
	}

	@Override
	public MmUser getAuthoriztionDetailsByEmail(String emailAddr) throws Exception {
		Operations op = ops.beginTransaction();
		try {
			CoreQuery coreQuery = new CoreQuery("from MmUser where emailAddr = :emailAddr",true);
			coreQuery.addParam("emailAddr", emailAddr);
			
			@SuppressWarnings("unchecked")
			List<MmUser> lst = (List<MmUser>) ops.fetch(coreQuery, op);
			ops.commitTransaction(op);
			if(Validator.validateList(lst)){
				return lst.get(0);
			}
			return null;
		} catch (Exception e) {
			e.printStackTrace();
			ops.rollBackTransaction(op);
			throw e;
		}
	}

	@Override
	public MmUser getAuthoriztionDetails(String emailAddr, String pwd) throws Exception {
		Operations op = ops.beginTransaction();
		try {
			CoreQuery coreQuery = new CoreQuery("from MmUser where emailAddr = :emailAddr and password = :password ",true);
			coreQuery.addParam("emailAddr", emailAddr);
			coreQuery.addParam("password", pwd);
			
			@SuppressWarnings("unchecked")
			List<MmUser> lst = (List<MmUser>) ops.fetch(coreQuery, op);
			ops.commitTransaction(op);
			if(Validator.validateList(lst)){
				return lst.get(0);
			}
			return null;
		} catch (Exception e) {
			e.printStackTrace();
			ops.rollBackTransaction(op);
			throw e;
		}
	}
}
