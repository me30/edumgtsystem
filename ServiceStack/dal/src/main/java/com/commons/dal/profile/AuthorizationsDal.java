package com.commons.dal.profile;

import com.commons.entity.MmUser;

public interface AuthorizationsDal {
	MmUser getAuthoriztionDetailsByEmail(String emailAddr) throws Exception;
	
	MmUser getAuthoriztionDetails(String emailAddr,String pwd) throws Exception;
}
