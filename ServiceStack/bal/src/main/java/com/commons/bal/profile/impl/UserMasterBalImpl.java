package com.commons.bal.profile.impl;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.commons.bal.profile.UserMasterBal;
import com.commons.dal.profile.AuthorizationsDal;
import com.commons.dal.profile.UserMasterDal;
import com.commons.entity.MmUser;
import com.commons.security.AppUserDetails;

@Component
public class UserMasterBalImpl implements UserMasterBal {

	@Autowired
	UserMasterDal userMasterDal;

	@Autowired
	AuthorizationsDal authorizationsDal;

	@Override
	public AppUserDetails userRegistration(AppUserDetails appUserDetails) throws Exception {
		
		if(!appUserDetails.getPassword().equals(appUserDetails.getConPassword())){
			throw new com.commons.exceptions.DataValidationException("Password is not match");
		}
		boolean isExist = userMasterDal.isUserExist(appUserDetails.getEmailAddr());
		AppUserDetails userDetails = null;
		if(!isExist){
			MmUser mmUser = new MmUser();
			mmUser.setId(appUserDetails.getId());
			mmUser.setFirstName(appUserDetails.getFirstName());
			mmUser.setMiddleName(appUserDetails.getMiddleName());
			mmUser.setLastName(appUserDetails.getLastName());
			mmUser.setEmailAddr(appUserDetails.getEmailAddr());
			mmUser.setPassword(appUserDetails.getPassword());
			mmUser.setSecurityQue1(appUserDetails.getSecurityQue1());
			mmUser.setSecurityAns1(appUserDetails.getSecurityAns1());
			mmUser.setSecurityQue2(appUserDetails.getSecurityQue2());
			mmUser.setSecurityAns2(appUserDetails.getSecurityAns2());
			mmUser.setGender(appUserDetails.getGender());
			mmUser.setProfilePicName(appUserDetails.getProfilePicName());
			mmUser.setAboutMe(appUserDetails.getAboutMe());
			mmUser.setPhoneWork(appUserDetails.getPhoneWork());
			mmUser.setPhoneHome(appUserDetails.getPhoneHome());
			mmUser.setRole("user");
			mmUser.setAcActive(appUserDetails.isAcActive());
			mmUser.setCreatedOn(new Date());
			mmUser.setAcActive(true);
			mmUser = userMasterDal.userRegistration(mmUser);
			
			if (null != mmUser) {
				userDetails = new AppUserDetails();
				userDetails.setId(mmUser.getId());
				userDetails.setFirstName(mmUser.getFirstName());
				userDetails.setMiddleName(mmUser.getMiddleName());
				userDetails.setLastName(mmUser.getLastName());
				userDetails.setEmailAddr(mmUser.getEmailAddr());
				userDetails.setPassword(mmUser.getPassword());
				userDetails.setSecurityQue1(mmUser.getSecurityQue1());
				userDetails.setSecurityAns1(mmUser.getSecurityAns1());
				userDetails.setSecurityQue2(mmUser.getSecurityQue2());
				userDetails.setSecurityAns2(mmUser.getSecurityAns2());
				userDetails.setGender(mmUser.getGender());
				userDetails.setProfilePicName(mmUser.getProfilePicName());
				userDetails.setAboutMe(mmUser.getAboutMe());
				userDetails.setPhoneWork(mmUser.getPhoneWork());
				userDetails.setPhoneHome(mmUser.getPhoneHome());
				userDetails.setRole("user");
				userDetails.setAcActive(mmUser.isAcActive());
			}
		}else{
			throw new com.commons.exceptions.DuplicateEntryException("User already Exists");
		}
		return userDetails;
	}
}
