package com.commons.bal.profile;

import com.commons.security.AppUserDetails;

public interface AuthorizationsBal {
	AppUserDetails getAuthoriztionDetailsByEmail(String emailAddr) throws Exception;
	
	AppUserDetails getAuthoriztionDetails(String emailAddr,String pwd) throws Exception;
}
