package com.commons.bal.profile.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.commons.bal.profile.AuthorizationsBal;
import com.commons.dal.profile.AuthorizationsDal;
import com.commons.entity.MmUser;
import com.commons.security.AppUserDetails;

@Component
public class AuthorizationsBalImpl implements AuthorizationsBal{

	@Autowired
	AuthorizationsDal authorizationsDal;
	
	@Override
	public AppUserDetails getAuthoriztionDetailsByEmail(String emailAddr) throws Exception {
		MmUser mUser = authorizationsDal.getAuthoriztionDetailsByEmail(emailAddr);
		AppUserDetails userDetails = null;
		if (null != mUser) {
			userDetails = new AppUserDetails();
			userDetails.setId(mUser.getId());
			userDetails.setFirstName(mUser.getFirstName());
			userDetails.setMiddleName(mUser.getMiddleName());
			userDetails.setLastName(mUser.getLastName());
			userDetails.setEmailAddr(mUser.getEmailAddr());
			userDetails.setPassword(mUser.getPassword());
			userDetails.setSecurityQue1(mUser.getSecurityAns1());
			userDetails.setSecurityAns1(mUser.getSecurityAns1());
			userDetails.setSecurityQue1(mUser.getSecurityAns2());
			userDetails.setSecurityAns1(mUser.getSecurityAns2());
			userDetails.setGender(mUser.getGender());
			userDetails.setProfilePicName(mUser.getProfilePicName());
			userDetails.setAboutMe(mUser.getAboutMe());
			userDetails.setPhoneWork(mUser.getPhoneWork());
			userDetails.setPhoneHome(mUser.getPhoneHome());
			userDetails.setRole(mUser.getRole());
			userDetails.setAcActive(mUser.isAcActive());
		}
		return userDetails;
	}

	@Override
	public AppUserDetails getAuthoriztionDetails(String emailAddr, String pwd)
			throws Exception {
		MmUser mUser = authorizationsDal.getAuthoriztionDetails(emailAddr,pwd);
		AppUserDetails userDetails = null;
		if (null != mUser) {
			userDetails = new AppUserDetails();
			userDetails.setId(mUser.getId());
			userDetails.setFirstName(mUser.getFirstName());
			userDetails.setMiddleName(mUser.getMiddleName());
			userDetails.setLastName(mUser.getLastName());
			userDetails.setEmailAddr(mUser.getEmailAddr());
			userDetails.setPassword(mUser.getPassword());
			userDetails.setSecurityQue1(mUser.getSecurityAns1());
			userDetails.setSecurityAns1(mUser.getSecurityAns1());
			userDetails.setSecurityQue1(mUser.getSecurityAns2());
			userDetails.setSecurityAns1(mUser.getSecurityAns2());
			userDetails.setGender(mUser.getGender());
			userDetails.setProfilePicName(mUser.getProfilePicName());
			userDetails.setAboutMe(mUser.getAboutMe());
			userDetails.setPhoneWork(mUser.getPhoneWork());
			userDetails.setPhoneHome(mUser.getPhoneHome());
			userDetails.setRole(mUser.getRole());
			userDetails.setAcActive(mUser.isAcActive());
		}
		return userDetails;
	}

}
