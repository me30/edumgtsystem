package com.commons.bal.profile;

import com.commons.security.AppUserDetails;


public interface UserMasterBal {
	AppUserDetails userRegistration(AppUserDetails appUserDetails) throws Exception;
}
