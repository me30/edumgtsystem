package com.commons.security;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;

import com.commons.bal.profile.AuthorizationsBal;

@Component
public class UserAuthenticationProvider implements AuthenticationProvider
{
    @Autowired
    private AuthorizationsBal authorizationsBal;

    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException
    {
	String userName = authentication.getName();
	String pwd = (String) authentication.getCredentials();

	AppUserDetails appUserDetails;
	try
	{
		appUserDetails =  authorizationsBal.getAuthoriztionDetails(userName, pwd);
	}
	catch (Exception e)
	{
	    // TODO Auto-generated catch block
	    throw new UsernameNotFoundException("Invalid username/password");
	}
	
	Collection<? extends GrantedAuthority> authorities = UserAuthorityUtils.createAuthorities(appUserDetails);
	DomainUsernamePasswordAuthenticationToken authR = new DomainUsernamePasswordAuthenticationToken(appUserDetails,pwd, authorities);
	return authR;
    }

    @Override
    public boolean supports(Class<?> authentication)
    {
	return DomainUsernamePasswordAuthenticationToken.class.equals(authentication);
    }

}
