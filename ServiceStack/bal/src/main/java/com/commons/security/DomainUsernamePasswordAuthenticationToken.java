package com.commons.security;

import java.util.Collection;

import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;

public class DomainUsernamePasswordAuthenticationToken extends UsernamePasswordAuthenticationToken
{
    // private final String domain;

    public DomainUsernamePasswordAuthenticationToken(String principal, String credentials)
    {
	super(principal, credentials);
    }

    public DomainUsernamePasswordAuthenticationToken(AppUserDetails principal, String credentials,
	    Collection<? extends GrantedAuthority> authorities)
    {
	super(principal, credentials, authorities);
    }

    private static final long serialVersionUID = -5138870746127783L;
}
