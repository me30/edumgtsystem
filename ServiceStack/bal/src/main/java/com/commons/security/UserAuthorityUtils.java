package com.commons.security;

import java.util.Collection;
import java.util.List;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;

import com.commons.utility.Constants;


public class UserAuthorityUtils
{

    private static final List<GrantedAuthority> ADMIN = AuthorityUtils.createAuthorityList(Constants.ADMIN);

    private static final List<GrantedAuthority> USER = AuthorityUtils.createAuthorityList(Constants.USER);

    public static Collection<? extends GrantedAuthority> createAuthorities (AppUserDetails user)
    {
        String roleName = user.getRole();

        if (roleName.equals(Constants.ADMIN))
        {
            return ADMIN;
        }
        else if (roleName.equals(Constants.USER))
        {
            return USER;
        }
       
        return null;
    }

    private UserAuthorityUtils ()
    {
    }
}
