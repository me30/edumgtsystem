package com.commons.security;

import java.util.Collection;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

public class AppUserDetails implements UserDetails {

	private Long id;
	private String firstName;
	private String middleName;
	private String lastName;
	private String emailAddr;
	private String password;
	private String securityQue1;
	private String securityQue2;
	private String securityAns1;
	private String securityAns2;
	private String gender;
	private String profilePicName;
	private String aboutMe;
	private String phoneWork;
	private String phoneHome;
	private String role;
	private boolean acActive;
	private String token;
	private String conPassword;

	public AppUserDetails() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() {
		System.out.println("getAuthorities called");
		return UserAuthorityUtils.createAuthorities(this);
	}

	@Override
	public boolean isAccountNonExpired() {
		System.out.println("isAccountNonExpired called");
		return this.isAcActive();
	}

	@Override
	public boolean isAccountNonLocked() {
		System.out.println("isAccountNonLocked called");
		return true;
	}

	@Override
	public boolean isCredentialsNonExpired() {
		System.out.println("isCredentialsNonExpired called");
		return true;
	}

	@Override
	public boolean isEnabled() {
		System.out.println("isEnabled called");
		return true;
	}

	private static final long serialVersionUID = 3384436451564509032L;

	@Override
	public String getUsername() {
		return this.emailAddr;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getMiddleName() {
		return middleName;
	}

	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getEmailAddr() {
		return emailAddr;
	}

	public void setEmailAddr(String emailAddr) {
		this.emailAddr = emailAddr;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getSecurityQue1() {
		return securityQue1;
	}

	public void setSecurityQue1(String securityQue1) {
		this.securityQue1 = securityQue1;
	}

	public String getSecurityQue2() {
		return securityQue2;
	}

	public void setSecurityQue2(String securityQue2) {
		this.securityQue2 = securityQue2;
	}

	public String getSecurityAns1() {
		return securityAns1;
	}

	public void setSecurityAns1(String securityAns1) {
		this.securityAns1 = securityAns1;
	}

	public String getSecurityAns2() {
		return securityAns2;
	}

	public void setSecurityAns2(String securityAns2) {
		this.securityAns2 = securityAns2;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getProfilePicName() {
		return profilePicName;
	}

	public void setProfilePicName(String profilePicName) {
		this.profilePicName = profilePicName;
	}

	public String getAboutMe() {
		return aboutMe;
	}

	public void setAboutMe(String aboutMe) {
		this.aboutMe = aboutMe;
	}

	public String getPhoneWork() {
		return phoneWork;
	}

	public void setPhoneWork(String phoneWork) {
		this.phoneWork = phoneWork;
	}

	public String getPhoneHome() {
		return phoneHome;
	}

	public void setPhoneHome(String phoneHome) {
		this.phoneHome = phoneHome;
	}

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}

	public boolean isAcActive() {
		return acActive;
	}

	public void setAcActive(boolean acActive) {
		this.acActive = acActive;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public String getConPassword() {
		return conPassword;
	}

	public void setConPassword(String conPassword) {
		this.conPassword = conPassword;
	}

}
