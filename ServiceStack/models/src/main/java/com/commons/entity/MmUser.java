package com.commons.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "user")
public class MmUser extends AbstractBaseEntity {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Column(name = "First_Name", length = 80)
	private String firstName;

	@Column(name = "Middle_Name", length = 80)
	private String middleName;

	@Column(name = "Last_Name", length = 80)
	private String lastName;

	@Column(name = "User_Name", length = 80)
	private String userName;

	@Column(name = "Email_Addr", length = 60)
	private String emailAddr;

	@Column(name = "Password", length = 80)
	private String password;

	@Column(name = "Security_Que1", length = 80)
	private String securityQue1;

	@Column(name = "Security_Que2", length = 80)
	private String securityQue2;

	@Column(name = "Security_Ans1", length = 80)
	private String securityAns1;

	@Column(name = "Security_Ans2", length = 80)
	private String securityAns2;

	@Column(name = "Gender", length = 8)
	private String gender;

	@Column(name = "Profile_Pic_Name", length = 80)
	private String profilePicName;

	@Column(name = "About_Me", length = 80)
	private String aboutMe;

	@Column(name = "PH_Work", length = 20)
	private String phoneWork;

	@Column(name = "PH_Home", length = 20)
	private String phoneHome;

	@Column(name = "Role", length = 20)
	private String role;

	@Column(name = "Ac_Active")
	private boolean acActive;

	public MmUser() {
		super();
	}

	public MmUser(long id) {
		this.id = id;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getMiddleName() {
		return middleName;
	}

	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getEmailAddr() {
		return emailAddr;
	}

	public void setEmailAddr(String emailAddr) {
		this.emailAddr = emailAddr;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getSecurityQue1() {
		return securityQue1;
	}

	public void setSecurityQue1(String securityQue1) {
		this.securityQue1 = securityQue1;
	}

	public String getSecurityQue2() {
		return securityQue2;
	}

	public void setSecurityQue2(String securityQue2) {
		this.securityQue2 = securityQue2;
	}

	public String getSecurityAns1() {
		return securityAns1;
	}

	public void setSecurityAns1(String securityAns1) {
		this.securityAns1 = securityAns1;
	}

	public String getSecurityAns2() {
		return securityAns2;
	}

	public void setSecurityAns2(String securityAns2) {
		this.securityAns2 = securityAns2;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getProfilePicName() {
		return profilePicName;
	}

	public void setProfilePicName(String profilePicName) {
		this.profilePicName = profilePicName;
	}

	public String getAboutMe() {
		return aboutMe;
	}

	public void setAboutMe(String aboutMe) {
		this.aboutMe = aboutMe;
	}

	public String getPhoneWork() {
		return phoneWork;
	}

	public void setPhoneWork(String phoneWork) {
		this.phoneWork = phoneWork;
	}

	public String getPhoneHome() {
		return phoneHome;
	}

	public void setPhoneHome(String phoneHome) {
		this.phoneHome = phoneHome;
	}

	public boolean isAcActive() {
		return acActive;
	}

	public void setAcActive(boolean acActive) {
		this.acActive = acActive;
	}

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

}
