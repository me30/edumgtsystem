package com.commons.db.models;


public class CoreQueryParam {
    private String paramName;
    private Object paramValue;

    public CoreQueryParam(String paramName, Object paramValue) {
        this.paramName = paramName;
        this.paramValue = paramValue;
    }

    public String getParamName() {
        return paramName;
    }

    public Object getParamValue() {
        return paramValue;
    }
}
