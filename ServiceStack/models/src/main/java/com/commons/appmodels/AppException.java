package com.commons.appmodels;

public class AppException {

    private int errorCode;
    private String serverMessage;
    private String errorMessage;

    public AppException() {
    }

    public AppException(int errorCode, String serverMessage, String errorMessage) {
        this.errorCode = errorCode;
        this.serverMessage = serverMessage;
        this.errorMessage = errorMessage;
    }

    public AppException(int errorCode, String errorMessage) {
        this.errorCode = errorCode;
        this.errorMessage = errorMessage;
    }

    public int getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(int errorCode) {
        this.errorCode = errorCode;
    }

    public String getServerMessage() {
        return serverMessage;
    }

    public void setServerMessage(String serverMessage) {
        this.serverMessage = serverMessage;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }
}
