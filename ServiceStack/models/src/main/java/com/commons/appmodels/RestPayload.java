package com.commons.appmodels;


import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;


public class RestPayload implements Serializable{

    private static final long serialVersionUID = -4329617004242031635L;
    private int maxItems;
    private int page;
    private int inlineCount;
    private List<?> results;

    public RestPayload() {
        this.maxItems = 12;
        this.page = 1;
        this.inlineCount = 0;
        this.results = new ArrayList<Object>();
    }

    public RestPayload(int page, int maxItems) {
        this.maxItems = maxItems;
        this.page = page;
        this.inlineCount = 0;
        this.results = new ArrayList<Object>();
    }

    public int getMaxItems() {
        return maxItems;
    }

    public void setMaxItems(int maxItems) {
        this.maxItems = maxItems;
    }

    public int getPage() {
        return page;
    }

    public void setPage(int page) {
        this.page = page;
    }

    public int getInlineCount() {
        return inlineCount;
    }

    public void setInlineCount(int inlineCount) {
        this.inlineCount = inlineCount;
    }

    public List<?> getResults() {
        return results;
    }

    public void setResults(List<?> results) {
        this.results = results;
    }
}
