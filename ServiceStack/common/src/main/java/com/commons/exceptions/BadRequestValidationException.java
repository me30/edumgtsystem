package com.commons.exceptions;

public class BadRequestValidationException extends Exception {
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public BadRequestValidationException(String message) {
        super(message);
    }
}
