package com.commons.utility;

import java.security.SecureRandom;
import java.text.MessageFormat;
import java.util.Locale;
import java.util.ResourceBundle;

import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.text.WordUtils;


public class RandomString {

    private static final char[] availChars;

    static {
        StringBuilder stringBuilder = new StringBuilder();
        for (char ch = '0'; ch <= '9'; ++ch) {
            stringBuilder.append(ch);
        }
        for (char ch = 'a'; ch <= 'z'; ++ch) {
            stringBuilder.append(ch);
        }
        availChars = stringBuilder.toString().toCharArray();
    }

    private final SecureRandom random = new SecureRandom();

    private final char[] generatedChars;

    public RandomString(int length) {
        if (length < 1) {
            throw new IllegalArgumentException("length < 1: " + length);
        }
        generatedChars = new char[length];
    }

    public String nextString() {
        for (int idx = 0; idx < generatedChars.length; ++idx) {
            generatedChars[idx] = availChars[random.nextInt(availChars.length)];
        }
        return new String(generatedChars);
    }

    public static String generateRandomAlphaNumericString() {
        return RandomStringUtils.random(32, 0, 20, true, true, "bj81G5RDED3DC6142kasok".toCharArray());
    }
}
